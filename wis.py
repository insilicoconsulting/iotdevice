#!/usr/bin/python
# coding=utf-8
import argparse
import base64
import hmac
import hashlib
import platform
import socket
import requests
import urllib
import json
import time
import datetime
import sys
import collections
sys.path.append('/usr/local/bin/dong-wis-service/')


import iothub_service_client
from iothub_service_client import IoTHubDeviceTwin, IoTHubError
from iothub_service_client_args import get_iothub_opt, OptionError

# import Yoctopuce Pyhton library (installed form PyPI)
from yoctopuce.yocto_api import *
from yoctopuce.yocto_temperature import *
from yoctopuce.yocto_humidity import *
from yoctopuce.yocto_gps import *
from yoctopuce.yocto_latitude import *
from yoctopuce.yocto_longitude import *


LOCAL_BUFFER_SIZE = 8640 # 8640 = 24 hours * 60 minutes * 6 (every 10 secounds)

class D2CMsgSender:
    API_VERSION = '2016-02-03'
    TOKEN_VALID_SECS = 10
    TOKEN_FORMAT = 'SharedAccessSignature sig=%s&se=%s&skn=%s&sr=%s'

    def __init__(self, device_id, iot_host, key):
        self.devicid = device_id
        self.iotHost = iot_host
        self.keyValue = key

    def _buildExpiryOn(self):
        return '%d' % (time.time() + self.TOKEN_VALID_SECS)

    def _buildIoTHubSasToken(self):
        resourceUri = '%s/devices/%s' % (self.iotHost, self.devicid)
        targetUri = resourceUri.lower()
        expiryTime = self._buildExpiryOn()
        toSign = '%s\n%s' % (targetUri, expiryTime)
        key = base64.b64decode(self.keyValue.encode('utf-8'))
        signature = urllib.quote(
            base64.b64encode(
                hmac.HMAC(key, toSign.encode('utf-8'), hashlib.sha256).digest()
            )
        ).replace('/', '%2F')
        return self.TOKEN_FORMAT % (signature, expiryTime, "", targetUri)

    def sendD2CMsg(self, message):
        sasToken = self._buildIoTHubSasToken()
        url = 'https://%s/devices/%s/messages/events?api-version=%s' % (self.iotHost, self.devicid, self.API_VERSION)
        try:
            r = requests.post(url, headers={'Authorization': sasToken}, data=message)
            return r.text, r.status_code
        except:
            return 'Error', 404 # This is essentially not a 404 but due to network issues

class IoTHub:
    connectionString = ""
    deviceId = ""

    uploadFrequencyMS = 6000  # default
    location = ""

    def __init__(self, connectionString, deviceId):
        self.connectionString = connectionString
        self.deviceId = deviceId

    def updateProperties(self):
        iothub_twin_method = IoTHubDeviceTwin(self.connectionString)
        try:
            twin_info = json.loads(iothub_twin_method.get_twin(self.deviceId))
            desiredProperties = twin_info["properties"]["desired"]
            self.uploadFrequencyMS = desiredProperties["telemetryInterval"]*1000
            self.location = desiredProperties["location"]
        except:
            print "Couldn't find properties in IoT hub or connect to IoT hub"

def readSensors(azure_deviceId):

    sensor = YSensor.FirstSensor()
    errmsg = YRefParam()

    while 1: #sensor.isOnline:
        sensorMeasurements = []

        while sensor is not None:
            try:
                sensorMeasurementName = sensor.get_friendlyName().split(".")[-1]
                sensorType = sensor.get_module().get_productName().split("-")[-1]
                sensorSerial = sensor.get_module().get_serialNumber().split("-")[-1]

                value = sensor.get_currentValue()
                unit = sensor.get_unit()
                measurement = {'type':sensorType, 'sensorName':sensorMeasurementName, 'sensorUID':sensorSerial, 'value':value,'unit':unit}

                sensorMeasurements.append(measurement)
                sensor = sensor.nextSensor()
            except:
                print("Error")
                break

        print(json.dumps(sensorMeasurements, indent=4,separators=(',',': ')))
        print("####################################")

        if sensorMeasurements:
            iotHubMessage = json.dumps({
                'deviceId': azure_deviceId,
                'sensors': sensorMeasurements,
                'recordTime':datetime.datetime.utcnow().isoformat(),
                'recordLocation': iotHub.location
            })

            sendSensorValues(iotHubMessage)

        YAPI.Sleep(iotHub.uploadFrequencyMS)
        YAPI.UpdateDeviceList(errmsg)
        sensor = YSensor.FirstSensor()

def sendSensorValues(iotHubMessage):
    ret = d2cMsgSender.sendD2CMsg(iotHubMessage)
    if 200 <= ret[1] <= 206:
        try:
            with open('wisbuffer') as f:
                rawMessages = f.readlines()

            messages = [x.strip() for x in rawMessages]
            rets = [d2cMsgSender.sendD2CMsg(message) for message in messages]

            for idx, val in reversed(list(enumerate(rets))):
                if val[1]==204:
                    del rawMessages[idx]

            with open('wisbuffer','wb+') as f:
                [f.write(i) for i in rawMessages]

        except:
            pass
    else:
        print("unable to contact Azure Iot Hub:" + ret[0])
        d = collections.deque(maxlen=LOCAL_BUFFER_SIZE)
        try:
            with open('wisbuffer') as f:
                currentBuffer = f.readlines()

            [d.append(i) for i in currentBuffer]

        except:
            pass

        d.append(iotHubMessage+'\n')
        with open('wisbuffer','wb+') as f:
            [f.write(i) for i in d]

if __name__ == '__main__':
    # parser = argparse.ArgumentParser(description='Yoctopuce device to Azure Iot Suite brigde.')
    parser = argparse.ArgumentParser(description='Yoctopuce device to Azure Iot Suite brigde', fromfile_prefix_chars='@')
    parser.add_argument('deviceId', type=str, help='The Device Id ')
    parser.add_argument('AccessKey', type=str, help='The access key of the device')
    parser.add_argument('HostName', type=str, help='The hostname of the Azure IoT Hub')
    parser.add_argument('ConnectionString', type=str, help='The connection string for device properties update')

    args = parser.parse_args()

    errmsg = YRefParam()

    # Setup the API to use local USB devices
    if YAPI.RegisterHub("usb", errmsg) != YAPI.SUCCESS:
        sys.exit("init error" + errmsg.value)

    deviceId = args.deviceId
    host_name = args.HostName
    connectionString = args.ConnectionString

    if not host_name.endswith(".azure-devices.net"):
        host_name += ".azure-devices.net"

    d2cMsgSender = D2CMsgSender(deviceId, host_name, args.AccessKey)

    try:
        (connectionString, deviceId) = get_iothub_opt(sys.argv[1:], connectionString, deviceId)
    except OptionError as option_error:
        print ( option_error )

    iotHub = IoTHub(connectionString, deviceId)

    # Update device properties
    # Currently only do this here - every time counts as a message to the IoT Hub
    iotHub.updateProperties()

    readSensors(deviceId)
    YAPI.FreeAPI()
    print("exiting..")
