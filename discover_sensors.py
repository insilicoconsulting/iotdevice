#!/usr/bin/python
#coding: utf-8

import os, sys
from yoctopuce.yocto_api import *
from yoctopuce.yocto_temperature import *

print("Look for connected usb devices..")

def die(msg):
    sys.exit(msg + " (Check USB connection)")

errmsg = YRefParam()

# Setup the API to use local USB devices
def discoverDevices():
    if YAPI.RegisterHub("usb", errmsg) != YAPI.SUCCESS:
        sys.exit("init error " + str(errmsg))

    print('Sensor list')

    sensor = YSensor.FirstSensor()

    while sensor is not None:
        sensorFunctionName = sensor.get_friendlyName().split(".")[-1]
        measurementName = sensor.get_module().get_productName().split("-")[-1]
        print(measurementName, sensorFunctionName)
        print('Value:' , sensor.get_currentValue(), sensor.get_unit())
        #print(sensor.get_module().functionValue(1))

        sensor = sensor.nextSensor()

    YAPI.FreeAPI()


def getTemp():
    print("DISCOVER")

    if YAPI.RegisterHub("usb", errmsg) != YAPI.SUCCESS:
        sys.exit("init error " + errmsg.value)

    sensor = YTemperature.FirstTemperature()
    if sensor is None:
        die("No module connected")  #dont do this in real life

    if not (sensor.isOnline()):
        die("No device connected")  # dont do this in real life

    print("Sensor found")
    serial = sensor.get_module().get_serialNumber()

    channel1 = YTemperature.FindTemperature(serial + ".temperature1")

    print("Data: " + "%2.1f" % channel1.get_currentValue())

    YAPI.FreeAPI()

discoverDevices()
